const { DataTypes } = require('sequelize');
module.exports = (sequelize) => {
    return sequelize.define('groups_messages', {
        id:{
          type:DataTypes.INTEGER,
          autoIncrement:true,
          unique:true,
          primaryKey:true
        },
        date: {
          type: DataTypes.DATE
        },
        timestamp: {
            type: DataTypes.BIGINT,
          },
        text:{
          type:DataTypes.TEXT
        },
        author_id:{
          type:DataTypes.BIGINT
        },
        group_id:{
          type:DataTypes.BIGINT
        },
        message_id:{
          type:DataTypes.BIGINT,
        } ,
        group:{
          type:DataTypes.STRING
        }
    },
    {
        indexes:[
         {
           unique: false,
           fields:['author_id','group','message_id']
         }
        ]
      })
}