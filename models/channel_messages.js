const { DataTypes } = require('sequelize');
module.exports = (sequelize) => {
    return sequelize.define('channel_messages', {
      id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        unique:true,
        primaryKey:true
      },
        date: {
          type: DataTypes.DATE
        },
        timestamp: {
            type: DataTypes.BIGINT,
          },
        message_id:{
          type:DataTypes.BIGINT,
        },
        channel_name:{
          type:DataTypes.STRING
        },  
        channel_id:{
          type:DataTypes.BIGINT
        },
        like:{
          type:DataTypes.INTEGER
        },
        dislike:{
          type:DataTypes.INTEGER
        },
        viewed:{
          type:DataTypes.INTEGER
        },
        comments:{
          type:DataTypes.INTEGER
        }
    },
    {
        indexes:[
         {
           unique: false,
           fields:['channel_id','message_id']
         }
        ]
      })
}