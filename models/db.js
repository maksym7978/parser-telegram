const Sequelize = require('sequelize')
const channel_messages = require('./channel_messages');
const groups_messages = require('./groups_messages');
require('dotenv').config()

const sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    syncOnAssociation:false,
    dialectOptions: {
      charset: 'utf8mb4',
      collate: 'utf8mb4_general_ci'
    }
  });

const groups = groups_messages(sequelize)
const channel = channel_messages(sequelize)
sequelize.query("SET NAMES utf8mb4;");
sequelize.query("SET CHARACTER SET utf8mb4");
sequelize.sync({ force: false })
  .then(() => {
    console.log(`База данных и таблицы созданы`)
  })
  
module.exports = {
    groups,
    channel
}