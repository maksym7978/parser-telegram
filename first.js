const { MTProto } = require('@mtproto/core');
require('dotenv').config()
const { tempLocalStorage } = require('@mtproto/core/src/storage/temp');
const Auth = require('./controllers/auth')
const {LocalStorage} = require('node-localstorage')
var prompt = require('prompt-sync')();
const api_id = process.env.TELEGRAM_API;
const api_hash = process.env.TELEGRAM_HASH;
const parsGroup = require('./controllers/parsGroup')
const parsChannel = require('./controllers/parsChannel')
const Search = require('./helpers/search');
const fs = require('fs');
const db = require('./models/db')
const mtproto = new MTProto({
  api_id,
  api_hash,
  customLocalStorage: new LocalStorage('./storage'),
});

mtproto.updateInitConnectionParams({
  app_version: '10.0.0',
});
var auth = new Auth(); 
async function run(){

      const user = await auth.auth(mtproto);
      if(user){
          console.log('Аккаунт сохранен')
      }else{
          console.log('Ошибка попробуйте снова')
          run();
      }
      
}
run()
