class Search{
    async getChannel(api,name){
        let channels = await api.call('contacts.search',{
            q:name,
            limit:1
          })

          return channels.chats.filter((val)=>{return val.username == name}).shift()
    }
}
module.exports = Search