const { MTProto } = require('@mtproto/core');
require('dotenv').config()
const { tempLocalStorage } = require('@mtproto/core/src/storage/temp');
const Auth = require('./controllers/auth')
const {LocalStorage} = require('node-localstorage')
var prompt = require('prompt-sync')();
const api_id = process.env.TELEGRAM_API;
const api_hash = process.env.TELEGRAM_HASH;
const parsGroup = require('./controllers/parsGroup')
const parsChannel = require('./controllers/parsChannel')
const Search = require('./helpers/search');
const fs = require('fs');

const mtproto = new MTProto({
  api_id,
  api_hash,
  customLocalStorage: new LocalStorage('./storage'),
});

mtproto.updateInitConnectionParams({
  app_version: '10.0.0',
});
var auth = new Auth(); 
async function run(){

      const user = await auth.auth(mtproto);
      let rawdata = fs.readFileSync('pars.json');
      let names = JSON.parse(rawdata);

      names.forEach(async name => {
        let group = await new Search().getChannel(mtproto, name)
        if(group.megagroup){
          new parsGroup().pars(mtproto, group)
        }else{
          new parsChannel().pars(mtproto, group)
        }
        
      });
      setTimeout(() => {
        run();
      },process.env.TIMES*60*1000)
}
run()
