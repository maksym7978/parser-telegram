var prompt = require('prompt-sync')();
class Auth{
    constructor(){}

    async  getUser(api) {
        try {
          const user = await api.call('users.getFullUser', {
            id: {
              _: 'inputUserSelf',
            },
          });
      
          return user;
        } catch (error) {
          console.log(error)
          return null;
        }
      }


       async sendCode(api,phone) {
        return api.call('auth.sendCode', {
          phone_number: phone,
          settings: {
            _: 'codeSettings',
          },
        })
        
        
      }

       async signIn(api,{ code, phone, phone_code_hash }) {
        return api.call('auth.signIn', {
          phone_code: code,
          phone_number: phone,
          phone_code_hash: phone_code_hash,
        }).catch((e) => {console.log(e)});
      }

      async auth(mtproto){
        const user = await this.getUser(mtproto);
        var phone = process.env.PHONE
        if (!user) {
          const { phone_code_hash } = await this.sendCode(mtproto,phone).catch((e) => {console.log(e)});
          
          let code =  prompt('enter code: ');
          try {
            const authResult = await this.signIn(mtproto,{
              code,
              phone,
              phone_code_hash,
            });
            
            return authResult
          } catch (error) {
              console.log(error)
            if (error.error_message !== 'SESSION_PASSWORD_NEEDED') {
              return false;
            }
      }
      }
      return user
      }
}
module.exports = Auth