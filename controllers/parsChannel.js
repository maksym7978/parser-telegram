const db = require('../models/db')
var moment = require('moment')
class parsChannel{
    async pars(api, group) {
        let messages = await api.call('messages.getHistory',{
            peer:{
                _:'inputPeerChannel',
                channel_id:group.id,
                access_hash:group.access_hash
            },
            limit:100
        }).catch((e) => console.log(e))

         messages = messages.messages.reverse()
      
        messages.forEach(async message => {
            let comments = 0;
            let like = 0;
            let dislike = 0;
            if(message.replies && message.replies.comments){
                comments = message.replies.replies
            }
            if(message.reply_markup){
                like = parseInt(message.reply_markup.rows[0].buttons[0].text.replace(/\D+/g,""))
                dislike = parseInt(message.reply_markup.rows[0].buttons[1].text.replace(/\D+/g,""))
               
            }
            let ch = await db.channel.findAll({where:{
                channel_id:message.peer_id.channel_id,
                message_id:message.id
            }})
            if(ch){
                await ch.destroy()
            }

            await db.channel.upsert({
                date:moment(message.date*1000).format("YYYY-MM-DD HH:mm:ss"),
                timestamp:message.date,
                channel_id:message.peer_id.channel_id,
                message_id:message.id,
                channel_name:group.username,
                viewed:message.views,
                like:like,
                dislike:dislike,
                comments:comments
            }).catch((e)=>console.log(e))
        });
        
    }
}

module.exports = parsChannel